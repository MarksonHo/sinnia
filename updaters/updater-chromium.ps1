#Versions
# $Version_New = curl -sL "https://api.github.com/repos/Hibbiki/chromium-win64/releases/latest" | Select-String -Pattern "tag_name" | ForEach-Object { ([string]$_).Split('"')[3] } | Select-Object -First 1 | ForEach-Object { ([string]$_).Split('-')[0] } | ForEach-Object { ([string]$_).Split('v')[1] }
# $Version_Old = Get-Content -Path "./bucket/chromium-browser.json" | Select-String -Pattern "version" | ForEach-Object { ([string]$_).Split('"')[3] }
$Version_New_Full = curl -sL "https://api.github.com/repos/Hibbiki/chromium-win64/releases/latest" | Select-String -Pattern "tag_name" | ForEach-Object { ([string]$_).Split('"')[3] } | Select-Object -First 1
$Version_Old_Full = Get-Content -Path ./bucket/chromium-browser.json | Select-String -Pattern "url" |  Select-Object -First 1 |  ForEach-Object { ([string]$_).Split('/')[7] }

#Hashes
$Hash_Old_64 = Get-Content -Path ./bucket/chromium-browser.json | Select-String -Pattern "hash" |  Select-Object -First 1 |  ForEach-Object { ([string]$_).Split('"')[3] }
$Hash_Old_32 = Get-Content -Path ./bucket/chromium-browser.json | Select-String -Pattern "hash" |  Select-Object -Last 1 |  ForEach-Object { ([string]$_).Split('"')[3] }
$Hash_New_64 = "sha1:" + $(curl -sL "https://api.github.com/repos/Hibbiki/chromium-win64/releases/latest" | Select-String body | ForEach-Object { ([string]$_).Split('"')[3] } | ForEach-Object { ([string]$_).Split(' ')[0] })
$Hash_New_32= "sha1:" + $(curl -sL "https://api.github.com/repos/Hibbiki/chromium-win32/releases/latest" | Select-String body | ForEach-Object { ([string]$_).Split('"')[3] } | ForEach-Object { ([string]$_).Split(' ')[0] })

#Path
$Chromium_JSON_Path = Resolve-Path -Path .\bucket\chromium-browser.json | ForEach-Object { ([string]$_)}

function Replace (){  
    (Get-Content $Chromium_JSON_Path) -replace "$Version_Old_Full", "$Version_New_Full" | out-file $Chromium_JSON_Path
#   (Get-Content $Chromium_JSON_Path) -Replace "$Version_Old", "$Version_New" | out-file $Chromium_JSON_Path
    (Get-Content $Chromium_JSON_Path) -replace "$Hash_Old_64", "$Hash_New_64" | out-file $Chromium_JSON_Path
    (Get-Content $Chromium_JSON_Path) -replace "$Hash_Old_32", "$Hash_New_32" | out-file $Chromium_JSON_Path
}

if ($Version_Old_Full -eq $Version_New_Full) {
    Write-Output "You have latest Chromium Browser!"
} else {
    Replace
    Write-Host "Chromium browser has updated to version $Version_New_Full!"
    git commit -a -m "Chromium Browser: updated to version $Version_New_Full"
}