# Sinnia - A Scoop bucket for myself

Sinnia is a Scoop bucket for myself to provide proxy tools，optimized for users in Mainland China via [FastGit](https://fastgit.org/).

## Installation of Scoop

### Via FastGit

```ps1
iwr -useb https://gitlab.com/MarksonHo/sinnia/-/raw/main/scoop-installer.ps1 | iex
```

### Via Gitee's mirror

<https://gitee.com/RubyKids/scoop-cn>

## PowerShell 7

To run scoop correctly, you should install PowerShell to run scoop instead of using Windows PowerShell.

+ [Download PowerShell from Microsoft Store](https://www.microsoft.com/en-us/p/powershell/9mz1snwt0n5d)
+ If you don't wanna download PowerShell from Microsoft Store, you can [Download it from GitHub](https://github.com/PowerShell/PowerShell).

## Microsoft .Net Desktop Runtime

Forked from <https://github.com/chawyehsu/dorado/blob/master/bucket/dotnet-desktop-runtime.json> to make sure that Clash .NET runs well.

## Other Scoop buckets

+ Kid's sushi: <https://github.com/kidonng/sushi>
+ Dorado: <https://github.com/chawyehsu/dorado>

## PowerShell proxy envs

Set PowerShell proxy envs to Shadowsocks Windows:

```bash
 $env:http_proxy="http://127.0.0.1:1080"; $env:https_proxy="http://127.0.0.1:1080"; $env:all_proxy="socks5://127.0.0.1:1080"
```