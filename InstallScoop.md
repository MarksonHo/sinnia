# How to install Scoop in China

China has blocked most features of GitHub, so you cannot install Scoop directly. As a solution, you might try to use [FastGit](http://fastgit.org/) to install scoop.

## Install Scoop

This repo contains a installer powered by FastGit, which makes you install Scoop easily.

```ps1
iwr -useb https://gitlab.com/MarksonHo/sinnia/-/raw/main/scoop-installer.ps1 | iex
```

## Install 7-zip

7-zip is necessary for Scoop, Scoop cannot unzip archive files if there is no 7-zip installed.

Modify `~\scoop\buckets\main\bucket\7zip.json`, change the download links to https://raw.fastgit.org:

E.g.

```json
"architecture": {
        "64bit": {
            "url": "https://raw.fastgit.org/ip7z/a/main/7z1900-x64.msi",
            "hash": "a7803233eedb6a4b59b3024ccf9292a6fffb94507dc998aa67c5b745d197a5dc"
        },
        "32bit": {
            "url": "https://raw.fastgit.org/ip7z/a/main/7z1900.msi",
            "hash": "b49d55a52bc0eab14947c8982c413d9be141c337da1368a24aa0484cbb5e89cd"
        }
    }
```

Then run command:

```ps1
scoop install 7zip git
```

